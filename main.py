__author__ = 'olivierdeckers'

import numpy as np
import cv2 as cv
import PCATeeth as pcat
import Procrustes as pc
import csv

def load_landmarks():
    landmarks = np.empty((14, 8, 40), dtype=(float, 2))
    for teeth in xrange(1,15):
        for tooth in xrange(1,9):
            f = open("_Data/Landmarks/original/landmarks" + `teeth` + "-" + `tooth` + ".txt", 'r')
            i = 0
            while True:
                line1 = f.readline()
                line2 = f.readline()
                if len(line1) == 0:
                    break

                landmarks[teeth-1][tooth-1][i] = (float(line1), float(line2))
                i+=1

    return landmarks

landmarks = load_landmarks()

def createPointsTransformed(array, diffx, diffy):
    result = np.empty(((array.shape[0]/2),), dtype=object)
    for j in xrange(0, array.shape[0]/2):
        #print (array[0,j*2] + diffx, array[0,j*2+1] + diffy)
        result[j] = (int(array[j*2] + diffx),int(array[j*2+1] + diffy))
        #result[j][1] = int(array[0,j*2+1] + diffy)
    return result



def generateForms(lambdanumber, PCAAnalysis, tooth):
    eigenvectors = PCAAnalysis.eigenVectors[tooth]
    image = np.zeros((600,850), np.uint8)
    testY = np.zeros(eigenvectors.shape[0])

    testx = 0.
    testy = 0.
    for j in xrange(40):
        testx += PCAAnalysis.means[tooth,j*2]
        testy += PCAAnalysis.means[tooth,j*2+1]
    testx /= 40
    testy /= 40

    xwidth = 6
    yheight = 6

    steps = 11

    dLambda = 1.0 / float(steps)

    for i in xrange(0,steps+1):
        testY[lambdanumber] = - .5 * np.sqrt(np.abs(PCAAnalysis.eigenValues[tooth,lambdanumber])) + dLambda * np.sqrt(np.abs(PCAAnalysis.eigenValues[tooth,lambdanumber])) * i
        print dLambda * i
        testLamba1 = PCAAnalysis.reconstruct(testY, PCAAnalysis.means[tooth, :], tooth)
        points = createPointsTransformed(testLamba1, -int(testx) + 100 + 120 * (i % xwidth), -int(testy) + 150 + 300 * int(i / yheight))
        for j in xrange(0,points.shape[0]):
            cv.line(image, points[j], points[(j + 1) % points.shape[0]], (255))

    cv.imshow("image lambda .5 " + `lambdanumber`, image)
    cv.imwrite("image lambda .5 " + `lambdanumber`+".png", image)
    cv.waitKey(0)

def generatePCAImagesToothOne():
    #mean = preprocess(landmarks[:,0,:,:])

    PCAAnalysis = pcat.PCATeeth(landmarks)

    generateForms(0, PCAAnalysis, 0)
    generateForms(1, PCAAnalysis, 0)
    generateForms(2, PCAAnalysis, 0)
    generateForms(3, PCAAnalysis, 0)

    # testY = np.zeros(eigenvectors.shape[0])
    # testY[1] = 100.
    # testLamba1 = reconstruct(eigenvectors,testY,mean)
    #
    # image = cv.imread("_Data/Radiographs/02.tif")
    #
    # for j in xrange(0,40):
    #     cv.circle(image, (int(landmarks[0,0,j,0]), int(landmarks[0,0,j,1])), 10, (255,0,0))
    #     cv.circle(image, (int(testLamba1[0,j*2]), int(testLamba1[0,j*2+1])), 10, (0,0,255))
    #     #cv.circle(image, (int(mean[j,0]), int(mean[j,1])), 10, (0,255,0))
    #
    # cv.imshow("image", image)
    # cv.waitKey(0)

def getNormal(model, landmark):
    pos = model[landmark]
    nextpos = model[(landmark+1)%40]
    prevpos = model[(landmark+39)%40]
    diff1 = nextpos - pos
    diff2 = pos - prevpos
    normal1 = np.array((diff1[1], -diff1[0]))
    normal2 = np.array((diff2[1], -diff2[0]))
    normal = (normal1 + normal2) / np.linalg.norm(normal1+normal2)
    return normal

def sampleAlongNormal(img, model, landmark, k):
    """
    Samples the grayscale at 2k+1 points around the landmark

    model is a list of landmarks for the given tooth
    landmark is the index of the landmark we want to sample around
    """
    pos = model[landmark]
    normal = getNormal(model, landmark)
    result = np.zeros(2*k+2, dtype=float)
    for i in xrange(-k, k+2):
        samplePos = pos + i*normal
        #cv.circle(img, tuple(map(int, samplePos)), 3, (255,255,255))
        result[i+k] = img[samplePos[1], samplePos[0]]

    # result = np.diff(result)
    # normalisationFactor = np.sum(np.abs(result))
    # if normalisationFactor > 0:
    #     result /= normalisationFactor
    return result

    #cv.line(image, tuple(map(int,pos)), tuple(map(int,pos+10*normal)), (255,255,255))
    #cv.circle(image, tuple(map(int, pos)), 3, (255,255,255))
    #cv.imshow("normals", cv.resize(image, (1400,900)))
    #cv.waitKey(0)

def load_image(teeth):
    filenumber = str(teeth + 1)
    if len(filenumber) < 2:
        filenumber = "0" + filenumber
    image = cv.imread("_Data/Radiographs/" + filenumber + ".tif", cv.CV_LOAD_IMAGE_GRAYSCALE)
    image = cv.GaussianBlur(image, (0,0), 2)
    image = cv.equalizeHist(image)
    return image

def trainModelEdgeProfiles(teeth_numbers, k=15):
    """
    Calculate a statistical model of the grayscale values around the landmarks
    """
    # samples[teeth, tooth, landmark, sampleIndex]
    samples = np.zeros((len(teeth_numbers),8,40,2*k+1), dtype=float)
    for teeth in range(len(teeth_numbers)):
        image = load_image(teeth_numbers[teeth])
        for tooth in xrange(0,8):
            for landmark in xrange(0, 40):
                model = landmarks[teeth, tooth, :]
                samples[teeth, tooth, landmark, :] = sampleAlongNormal(image, model, landmark, k)

    # means[tooth, landmark, sampleIndex]
    means = np.zeros((8, 40, 2*k+1), dtype=float)
    # for tooth in xrange(0, 8):
    #     for landmark in xrange(0, 40):
    #         for sampleIndex in xrange(0, 2*k+1):
    #             means[tooth, landmark, sampleIndex] = np.mean(samples[:, tooth, landmark, sampleIndex])

    covarMatrices = np.zeros((8, 40, 2*k+1, 2*k+1))
    for tooth in xrange(0, 8):
        for landmark in xrange(0, 40):
            (tempcovar, mean) = cv.calcCovarMatrix(samples[:, tooth, landmark, :].transpose(), cv.cv.CV_COVAR_NORMAL + cv.cv.CV_COVAR_COLS )
            (_,covarMatrices[tooth, landmark]) = cv.invert(tempcovar, flags=cv.DECOMP_SVD)
            means[tooth, landmark,:] = np.reshape(mean,(2*k+1))
            # (covarMatrices[tooth, landmark],means[tooth, landmark,:]) = cv.calcCovarMatrix(samples[:, tooth, landmark, :].transpose(), cv.cv.CV_COVAR_NORMAL + cv.cv.CV_COVAR_COLS )

    # print means2.shape
    # print means - means2

    return (means, covarMatrices)

def getDxs(image, model, means, covarMatrices, tooth, m):
    result = np.zeros((40,2))
    for landmark in xrange(40):
        sample = sampleAlongNormal(image, model, landmark, m)
        sample = np.diff(sample)
        max = 0
        for i in xrange(m+1):
            outside = sample[-i+m] * (m+1-i) / (m+1)
            if outside > max:
                max = outside
                result[landmark] = -i * getNormal(model, landmark)
            inside = sample[i+m] * (m+1-i) / (m+1)
            if inside > max:
                max = inside
                result[landmark] = i * getNormal(model, landmark)

    return result



# def getDxs(image, model, means, covarMatrices, tooth, m):
#     k = means.shape[2]
#
#     positionDeltas = np.zeros(())
#     # for tooth in xrange(0,8):
#     sampleResults = np.empty((40,2*m+1))
#     for landmark in xrange(0,40):
#         sampleResults[landmark,:] = sampleAlongNormal(image, model, landmark, m)
#
#
#     indices = np.empty((40,2))
#
#     for landmark in xrange(0,40):
#         minFit = float("inf")
#         index = -1
#         for i in xrange(2*m+1-k + 1):
#             tempsampleresults = sampleResults[landmark, i:i+k]
#             abssum = 0.0
#             for j in xrange(tempsampleresults.shape[0]):
#                 abssum += np.abs(tempsampleresults[j])
#             # tempsampleresults /= abssum
#
#             temp = cv.Mahalanobis(tempsampleresults, means[tooth, landmark, :], covarMatrices[tooth, landmark, :, :])
#             # temp = qualityOfFit(sampleResults[0,i:i+k],means[0,0,:],covarMatrices[0,0,:])
#             # print temp
#             if temp < minFit :
#                 minFit = temp
#                 index = i
#             # print i + (k/2) - m
#         # print covarMatrices
#         indices[landmark] = (index + (k/2) - m) * getNormal(model, landmark)
#     return indices

def getTransformMatrix(deformdict):
    rotation = deformdict["rotation"]
    scale = deformdict["scale"]
    translation1 = deformdict["translation1"]
    translation2 = deformdict["translation2"]


    scaleM = np.zeros((3,3))
    scaleM[0,0] = scale
    scaleM[1,1] = scale
    scaleM[2,2] = 1.0

    transM1 = np.zeros((3,3))
    transM1[0,0] = 1.0
    transM1[1,1] = 1.0
    transM1[2,2] = 1.0
    transM1[0,2] = translation1[0]
    transM1[1,2] = translation1[1]

    transM2 = np.zeros((3,3))
    transM2[0,0] = 1.0
    transM2[1,1] = 1.0
    transM2[2,2] = 1.0
    transM2[0,2] = translation2[0]
    transM2[1,2] = translation2[1]

    rotM = np.zeros((3,3))
    rotM[0,0] = rotation[0, 0]
    rotM[0,1] = rotation[0, 1]
    rotM[1,0] = rotation[1, 0]
    rotM[1,1] = rotation[1, 1]
    rotM[2,2] = 1.0

    return np.dot(transM2,np.dot(rotM,np.dot(scaleM,transM1)))


def drawModel(image, model, title):
    img = np.copy(image)
    for i in xrange(40):
        cv.circle(img, (int(model[i,0]), int(model[i, 1])), 2, (255,255,255))
    cv.imshow(title, cv.resize(img, (1440, 900)))
    #cv.waitKey(0)

def transformModel(matrix, model):
    newModel3Coor = np.hstack((model, np.ones((model.shape[0],1))))
    return (np.dot(matrix,newModel3Coor.transpose() )).transpose()[:,0:2]

def clipB(b, eigenValues):
    # Dmax = 3.0
    # Dm = 0.0
    # for j in xrange(14):
    #     Dm += b[j]**2 / np.abs(eigenValues[j])
    #
    # Dm = np.sqrt(Dm)
    # print Dm
    # if(Dm > Dmax):
    #     print "clipped"
    #     return b * (Dmax/Dm)
    # else:
    #     return b

    sqrtEig = np.sqrt(np.abs(eigenValues))
    for j in xrange(len(eigenValues)):
        b[j] = np.clip(b[j], -0.5*sqrtEig[j], 0.5*sqrtEig[j])
    return b

def methodWithSteadyDx(image, model, PCAAnalysis, tooth, DxModel, b, deformation):
    for i in xrange(20):
        x = np.reshape(model, (80, 1)) + np.dot(np.transpose(PCAAnalysis.eigenVectors[tooth]), b)
        x = np.reshape(x, (40, 2))

        transformedX = transformModel(deformation, x)
        #drawModel(image, newModel, "dx" + str(i))
        (_,tempModel,newDeformation) = pc.procrustes(DxModel, transformedX)

        deformation = np.dot(getTransformMatrix(newDeformation), deformation)

        # if i%10 == 0:
        #     drawModel(image, tempModel, "iteration" + str(i))

        deformationinv = np.linalg.inv(deformation)
        newModelInMeanSpace = transformModel(deformationinv, DxModel)

        # drawModel(image, newModelInMeanSpace, "MODELMEANSPACE"+ str(i))
        # drawModel(image, model, "MODEL" + str(i))

        oldb = np.copy(b)
        b = np.dot(PCAAnalysis.eigenVectors[tooth], np.reshape(newModelInMeanSpace - model, (80, 1)))


        #print "test: ", np.linalg.norm(b-oldb)

        b = clipB(b, PCAAnalysis.eigenValues[tooth, :])

        # print str(i) + ", " + str(np.linalg.norm(b-oldb))
        # print np.linalg.norm(b-oldb)
        if np.linalg.norm(b-oldb) / np.linalg.norm(b) < 0.002:
            return (tempModel, b, deformation)

    raise "not converged"
    #return (tempModel, b, deformation)

def computeOptimalModelDeformationOneTooth(image, model, means, covarMatrices, PCAAnalysis, tooth, m=30):
    b = np.zeros((PCAAnalysis.eigenValues.shape[1], 1))
    deformation = np.eye(3)

    for j in range(40):
        x = np.reshape(model, (80, 1)) + np.dot(np.transpose(PCAAnalysis.eigenVectors[tooth]), b)
        x = np.reshape(x, (40, 2))
        transformedX = transformModel(deformation, x)
        dx = getDxs(image, transformedX, means, covarMatrices, tooth, m)
        newModel = transformedX + dx
        # drawModel(image, newModel, "test" + str(j))
        # cv.waitKey(0)
        (tempmodel, b, deformation) = methodWithSteadyDx(image, model, PCAAnalysis, tooth, newModel, b, deformation)
        sum = 0
        for x in xrange(40):
            sum += np.sqrt(dx[x, 0] * dx[x, 0] + dx[x, 1] * dx[x, 1])
        sum /= 40
        print sum
        # print sum
        if sum < 1.0:
            print "gebreaked met som: ", sum
            break

    #if j >= 39:
    #    raise Exception("not converged")

    return tempmodel


    # for i in xrange(200):
    #     x = np.reshape(model, (80, 1)) + np.dot(np.transpose(PCAAnalysis.eigenVectors[tooth]), b)
    #     x = np.reshape(x, (40, 2))
    #
    #     transformedX = transformModel(deformation, x)
    #
    #
    #
    #
    #
    #
    #     # drawModel(image, newModel, "dx" + str(i))
    #     (_,tempModel,newDeformation) = pc.procrustes(newModel, transformedX)
    #
    #     deformation = np.dot(getTransformMatrix(newDeformation), deformation)
    #
    #     if i%10 == 0:
    #         drawModel(image, tempModel, "iteration" + str(i))
    #
    #     deformationinv = np.linalg.inv(deformation)
    #     newModelInMeanSpace = transformModel(deformationinv, newModel)
    #
    #     # drawModel(image, newModelInMeanSpace, "MODELMEANSPACE"+ str(i))
    #     # drawModel(image, model, "MODEL" + str(i))
    #
    #     oldb = b
    #     b = np.dot(PCAAnalysis.eigenVectors[tooth], np.reshape(newModelInMeanSpace - model, (80, 1)))
    #
    #
    #     #print "test: ", np.linalg.norm(b-oldb)
    #
    #     b = clipB(b, PCAAnalysis.eigenValues[tooth, :])
    #
    #
    #     # print str(i) + ", " + str(np.linalg.norm(b-oldb))
    #     # print np.linalg.norm(b-oldb)
    #     if np.linalg.norm(b-oldb) < 1e-8:
    #         print i
    #         return tempModel
    # else:
    #     print "not converged"
    #     return tempModel


def computeOptimal2PointsDeformation(image, point, direction,mean, covarMatrices):
    sampleAlongNormal(image,landmarks[0,0,:], 0, 10)

def compute_solution_error(solution, model):
    error = 0
    for i in xrange(solution.shape[0]):
        dx = solution[i, 0] - model[i, 0]
        dy = solution[i, 1] - model[i, 1]
        error += np.sqrt(dx*dx + dy*dy)
    return error / 40.0

def writeResults(results):
    with open('results.csv', 'wb') as csvfile:
        writer = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for i in range(14):
            writer.writerow(results[i])


def cross_validation():
    full_training_set = list(range(14))
    results = np.zeros((14, 8))
    for i in range(1, 2):
        training_set = full_training_set[:i] + full_training_set[i+1:]

        (mean, covariance) = trainModelEdgeProfiles(training_set)
        pca_analysis = pcat.PCATeeth(landmarks[training_set])

        image = load_image(i)

        for tooth in range(8):
            fittedModel = find_tooth_automatic(image, tooth, training_set, mean, covariance, pca_analysis)

            error = compute_solution_error(fittedModel, landmarks[i, tooth])
            results[i, tooth] = error
            print error

    writeResults(results)

def find_tooth(image, tooth, training_set, initialGuess, mean=None, covariance=None, pca_analysis=None):
    # if mean == None or covariance == None:
    #     (mean, covariance) = trainModelEdgeProfiles(training_set)

    if pca_analysis == None:
        pca_analysis = pcat.PCATeeth(landmarks[training_set])
    model = pca_analysis.means[tooth].reshape((40,2))
    meanX = np.mean(model[:,0])
    meanY = np.mean(model[:,1])
    (x,y) = initialGuess
    for i in xrange(40):
        model[i] += np.array([x-meanX, y-meanY])

    return computeOptimalModelDeformationOneTooth(image, model, mean, covariance, pca_analysis, tooth)

def find_tooth_manual(image, tooth, training_set):
    """
    Asks the user to click, giving an initial guess for the location of the tooth
    """
    cv.imshow("image", cv.resize(image, (1440, 900)))

    def onClickReturnPoint(event, x, y, flags, userdata):
        x *= image.shape[1] / 1440.0
        y *= image.shape[0] / 900.0

        if event == cv.cv.CV_EVENT_LBUTTONDOWN:
            fittedModel = find_tooth(image, tooth, training_set, (x,y))
            cv.circle(image, (int(x),int(y)), 5, (255,255,255))
            drawModel(image, fittedModel, "fittedModel")

    cv.cv.SetMouseCallback("image", onClickReturnPoint)
    cv.waitKey(0)

def mean_tooth_position(tooth, training_set):
    result = np.zeros((1,2))

    for i in training_set:
        result += np.mean(landmarks[i, tooth, :], axis=0)
    result /= len(training_set)

    return result

def correct_horizontal_position(image, initial_guess, m=100):
    (x,y) = initial_guess
    sample_right = image[y, x:x+m]
    sample_left = image[y, x-m:x]
    right = None
    left = None
    for i in range(m):
        if sample_right[i] < 100 and right is None:
            right = x + i
        if sample_left[i] < 100 and left is None:
            left = x - i

    if left is None or right is None:
        return (x,y)

    return (int(round((left + right) / 2.0)), y)



def find_tooth_automatic(image, tooth, training_set, mean=None, covariance=None, pca_analysis=None):
    """
    Automatically finds the starting point by using the tooth from the first radiograph as a template
    and finding the location of the tooth in the image that best fits this template
    """
    mean_pos = mean_tooth_position(tooth, training_set)
    result = []
    for i in training_set:
        points = np.zeros((40, 1, 2), dtype=np.int32)
        points[:] = np.reshape(landmarks[i, tooth, :], (40, 1, 2))
        (x, y, w, h) = cv.boundingRect(points)
        template = image[y:y+h,x:x+w]

        match = cv.matchTemplate(image, template, cv.cv.CV_TM_SQDIFF_NORMED)
        (min, _, minLoc, _) = cv.minMaxLoc(match)
        minLoc = (minLoc[0] + w/2, minLoc[1] + h/2)

        if min < 0.1:
            result.append((np.linalg.norm(minLoc - mean_pos), min, minLoc))

    result = sorted(result, key=lambda x: x[0])
    loc = result[0][2]
    # cv.circle(image, tuple(map(int, mean_pos[0])), 10, (255,255,255))
    # cv.circle(image, loc, 5, (255,255,255))
    #loc = correct_horizontal_position(image, loc)

    cv.circle(image, loc, 5, (255,255,255))

    return find_tooth(image, tooth, training_set, loc, mean, covariance, pca_analysis)

def find_teeth(id):
    training_set = [x for x in range(14) if x != id]
    image = load_image(id)
    newImage = np.copy(image)
    # (mean, covariance) = trainModelEdgeProfiles(training_set)
    pca_analysis = pcat.PCATeeth(landmarks[training_set])
    for tooth in xrange(8):
        fittedModel = find_tooth_automatic(image, tooth, training_set, None, None, pca_analysis)
        for i in xrange(40):
            cv.circle(newImage, (int(fittedModel[i,0]), int(fittedModel[i, 1])), 2, (255,255,255))

        cv.imshow("teeth", cv.resize(newImage, (1400, 900)))
        cv.waitKey(1)
    cv.waitKey(0)

def find_teeth_cheating(id):
    training_set = [x for x in range(14) if x != id]
    image = load_image(id)
    newImage = np.copy(image)
    # (mean, covariance) = trainModelEdgeProfiles(training_set)
    pca_analysis = pcat.PCATeeth(landmarks[training_set])
    for tooth in xrange(8):
        loc = tuple(map(int, np.mean(landmarks[id, tooth, :], axis=0)))
        fittedModel = find_tooth(image, tooth, training_set, loc, None, None, pca_analysis)
        for i in xrange(40):
            cv.circle(newImage, (int(fittedModel[i,0]), int(fittedModel[i, 1])), 2, (255,255,255))

        cv.imshow("teeth", cv.resize(newImage, (1400, 900)))
        cv.waitKey(1)
    cv.waitKey(0)

if __name__ == "__main__":
    find_teeth_cheating(4)
    # id = 0
    # training_set = [x for x in range(14) if x != id]
    # image = load_image(id)
    # fittedModel = find_tooth_manual(image, 1, training_set)#find_tooth_automatic(image, 7, training_set)
    # drawModel(image, fittedModel, "test")
    # cv.waitKey(0)


