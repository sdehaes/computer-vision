__author__ = 'stijndehaes'

import numpy as np
import Procrustes as pc

class PCATeeth:

    def __init__(self, landmarks):
        eigenValues = np.empty((8,landmarks.shape[0]))
        meansPCA = np.empty((8,80))
        eigenVectorsPCA = np.empty((8,landmarks.shape[0],80))
        for i in xrange(0,8):
            (eigenValues[i], eigenVectorsPCA[i], meansPCA[i]) = self.PCAOneTooth(landmarks[:,i,:,:])
        self.means = meansPCA
        self.eigenVectors = eigenVectorsPCA
        self.eigenValues = eigenValues

    def PCAOneTooth(self, models):
        self.preprocess(models)
        for i in xrange(models.shape[0]):
            test = models[i,:,:].reshape(80)
            try:
                matrix = np.vstack((matrix,test))
            except:
                matrix = test
        return pca(matrix)

    def preprocess(self, models):
        shape1 =  np.copy(models[0,:,:])
        (_,mean,_) = pc.procrustes(shape1,np.mean(models, axis=0))
        meanChangeThroughIterations = float ("inf")
        it = 0
        while(meanChangeThroughIterations > 1e-6):
            for i in xrange(0,models.shape[0]):
                (_,models[i,:,:],_) = pc.procrustes(mean,models[i,:,:])
            (_,newMean,_) = pc.procrustes(shape1,np.mean(models, axis=0))
            meanChangeThroughIterations = self.meanDifference(newMean,mean)
            mean = newMean
        return mean

    def meanDifference(self, newMean, oldMean):
        diff = 0.0
        for i in xrange(0,newMean.shape[0]):
            diff += np.linalg.norm(newMean[i,:] - oldMean[i,:])
        return diff
    # return cv.PCACompute(matrix)

    def reconstruct(self, Y, mu, tooth):
        '''
        Reconstruct an image based on its PCA-coefficients Y, the eigenvectors W and the average mu.
        '''
        result = np.copy(mu)
        times = Y.shape[0]
        for i in range(0,times):
            result += Y[i] * self.eigenVectors[tooth,i,:]

        return result
def pca(X, nb_components=0):
    '''
    Do a PCA analysis on X
    @param X:                np.array containing the samples
                             shape = (nb samples, nb dimensions of each sample)
    @param nb_components:    the nb components we're interested in
    @return: return the nb_components largest eigenvalues and eigenvectors of the covariance matrix and return the average sample
    '''
    [n,d] = X.shape
    if (nb_components <= 0) or (nb_components>n):
        nb_components = n

    mean = np.zeros(d)

    for i in range(0,n):
        mean += X[i,:]

    mean /= n

    for i in range(0,n):
        X[i,:] = X[i,:] - mean

    test = X.dot(np.transpose(X))
    [eigenValues, eigenVectors] = np.linalg.eig(test)
    idx = eigenValues.argsort()[::-1]
    eigenValues = eigenValues[idx]
    eigenVectors = eigenVectors[:,idx]
    eigenVectorsR = np.zeros((d,n))
    for i in range(0,n):
        eigenVectorsR[:,i] = np.transpose(X).dot(eigenVectors[:,i])
        eigenVectorsR[:,i] /= np.linalg.norm(eigenVectorsR[:,i])

    return (eigenValues,np.transpose(eigenVectorsR),mean)