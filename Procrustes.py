# __author__ = 'stijndehaes'
# import numpy as np
#
#
# # Source: http://stackoverflow.com/questions/18925181/procrustes-analysis-with-numpy
# def procrustes(X, Y, scaling=True, reflection='best'):
#     """
#     A port of MATLAB's `procrustes` function to Numpy.
#
#     Procrustes analysis determines a linear transformation (translation,
#     reflection, orthogonal rotation and scaling) of the points in Y to best
#     conform them to the points in matrix X, using the sum of squared errors
#     as the goodness of fit criterion.
#
#         d, Z, [tform] = procrustes(X, Y)
#
#     Inputs:
#     ------------
#     X, Y
#         matrices of target and input coordinates. they must have equal
#         numbers of  points (rows), but Y may have fewer dimensions
#         (columns) than X.
#
#     scaling
#         if False, the scaling component of the transformation is forced
#         to 1
#
#     reflection
#         if 'best' (default), the transformation solution may or may not
#         include a reflection component, depending on which fits the data
#         best. setting reflection to True or False forces a solution with
#         reflection or no reflection respectively.
#
#     Outputs
#     ------------
#     d
#         the residual sum of squared errors, normalized according to a
#         measure of the scale of X, ((X - X.mean(0))**2).sum()
#
#     Z
#         the matrix of transformed Y-values
#
#     tform
#         a dict specifying the rotation, translation and scaling that
#         maps X --> Y
#
#     """
#
#     n,m = X.shape
#     ny,my = Y.shape
#
#     muX = X.mean(0)
#     muY = Y.mean(0)
#
#     X0 = X - muX
#     Y0 = Y - muY
#
#     ssX = (X0**2.).sum()
#     ssY = (Y0**2.).sum()
#
#     # centred Frobenius norm
#     normX = np.sqrt(ssX)
#     normY = np.sqrt(ssY)
#
#     # scale to equal (unit) norm
#     X0 /= normX
#     Y0 /= normY
#
#     if my < m:
#         Y0 = np.concatenate((Y0, np.zeros(n, m-my)),0)
#
#     # optimum rotation matrix of Y
#     A = np.dot(X0.T, Y0)
#     U,s,Vt = np.linalg.svd(A,full_matrices=False)
#     V = Vt.T
#     T = np.dot(V, U.T)
#
#     if reflection is not 'best':
#
#         # does the current solution use a reflection?
#         have_reflection = np.linalg.det(T) < 0
#
#         # if that's not what was specified, force another reflection
#         if reflection != have_reflection:
#             V[:,-1] *= -1
#             s[-1] *= -1
#             T = np.dot(V, U.T)
#
#     traceTA = s.sum()
#
#     if scaling:
#
#         # optimum scaling of Y
#         b = traceTA * normX / normY
#
#         # standarised distance between X and b*Y*T + c
#         d = 1 - traceTA**2
#
#         # transformed coords
#         Z = normX*traceTA*np.dot(Y0, T) + muX
#
#     else:
#         b = 1
#         d = 1 + ssY/ssX - 2 * traceTA * normY / normX
#         Z = normY*np.dot(Y0, T) + muX
#
#     # transformation matrix
#     if my < m:
#         T = T[:my,:]
#     c = muX - b*np.dot(muY, T)
#
#     tform = {'rotation':T, 'scale':b, 'translation':c}
#
#     return d, Z, tform

from math import cos, sin, atan
from numpy import array, dot

def translate(points):
    """ This function translates the points to center around the origin. """

    mean = sum(points) / points.shape[0]
    translatedPoints = points - mean

    return (translatedPoints, mean)

def scale(points):
    """ This function scales the points. Assumes that points are already
        centered around the origin. """

    scale = sum(pow(sum(pow(points, 2.0) / float(points.shape[0])), .5))
    return (points / scale, scale)

def get_rotation(template, points):

    numerator = sum(points[:, 0] * template[:, 1] - \
                points[:, 1] * template[:, 0])

    divisor = sum(points[:, 0] * template[:, 0] + \
                points[:, 1] * template[:, 1])

    #   Avoiding dividing by zero
    if divisor == 0.0:
        divisor = 0.00000000001

    return atan(numerator / divisor)

def rotate(points, theta, center_point=(0, 0)):
    """
    Rotates the points around the center point.
    """

    new_array = array(points)

    new_array[0, :] -= center_point[0]
    new_array[1, :] -= center_point[1]

    new_array = dot(rotation_matrix_2D(theta),
                    new_array.transpose()).transpose()

    new_array[0, :] += center_point[0]
    new_array[1, :] += center_point[1]

    return new_array

def rotation_matrix_2D(theta):

    return array([[cos(theta), -sin(theta)], [sin(theta), cos(theta)]])

def procrustes(template, points):
    """
    This function computes the minizimed distance between the template
    and a test set of point once the test set has been scaled, translated,
    and rotated to the best fit.

    """

    (translatedPoints, mean1) = translate(points)
    (translatedTemplate, mean2) = translate(template)

    (scaledPoints, scale1) = scale(translatedPoints)
    (scaledTemplate, scale2) = scale(translatedTemplate)

    theta = get_rotation(scaledTemplate, scaledPoints)
    r_points = rotate(scaledPoints, theta)

    SSE = pow(pow(scaledTemplate - r_points, 2.0).sum(), .5)

    fittedModel = r_points * scale2 + mean2

    transform = {
        "translation2": mean2,
        "translation1": -mean1,
        "rotation": rotation_matrix_2D(theta),
        "scale": scale2/scale1,
    }

    return (SSE, fittedModel, transform)

if __name__ == '__main__':

    #   Define the template, just a triangle
    template = array([[-60., 0.],
                      [60., 20.],
                      [60., -20.],
                      [-60., 0.]])

    #   Points that are tested
    points = array([[50., 170.],
                    [70., 50.],
                    [30., 50.],
                    [50., 170.]])

    #   This is will be the function that we will call
    print 'procrustes distance:', procrustes(template, points)
